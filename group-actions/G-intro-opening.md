[Group] What should be open and why?
===

## Meta

- 30…90min
- People form groups of 3…5

## Plan

### 1st round | brainstorming: [15min]

- What should be open source?
- And why would that make this product special? → How is it better than e.g. a super cheap, but proprietary copy from china?

### 2nd round | ideate & challenge [∑=50min]

- Each group member formulates 1 own idea [10min]
- and pitches them to the other group members [5min each]
- group challenges the idea [5min each] with questions like:
    - Use case: What problem does it solve?
    - What is it exactly? What’s new, what can be reused from other designs? (this helps you finding your position in the ecosystem)
    - How is this better than existent solutions?
    - Target group: For whom are we building this?
    - … and with whom? (e.g. collaboration with manufacturers as Sparkfun Electronics)

**NOTE:** This obviously too less time for an exhaustive discussion; cutting the discussion off when it's most interesting will keep brains active & interested.

### 3rd round | group storming

- Each group decides on 1 idea to pitch.
- 1 member of each group presents the idea in 1min to the rest of the course.

**NOTE 1:** Strict time management.

**NOTE 2:** Questions students should have in mind after this:
- Can we collaborate within this course? This is already a community :)
- What is our position in the ecosystem? How can we contribute, how can we benefit from others?
