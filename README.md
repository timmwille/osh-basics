Bascis of Open Source Hardware
===

## General

This is a teaching module. Find its sections below.

Slides rely on [reveal.js](https://revealjs.com/) and are written in markdown.

To launch them in a presentable mode just copy the markdown source into a new document of any CodiMD instance (e.g. [here](http://demo.codimd.org/)).

## 12.11. Introduction

- What is this course about?
- presentation of all lecturers
- entrepreneurship basics
- Why, What, How OSH + examples
- potentials for entrepreneurship

**Plan:**

- Sigrid: Intro [15min]
- _ventilate_
- Sigrid: general introduction to Business Models [60min]
- _ventilate_
- Martin: Open Source Hardware [60min]
    - [slide show](https://pad2.opensourceecology.de/p/Sk4sMZeYD#/) ([source](slides/s-intro-osh.md))
- _ventilate_
- Sigrid: Group forming & discussion [90min]
    - [Group] What should be open and why?
        - [group action: opening](group-actions/G-intro-opening.md)

## 19.11. Documentation of OSH

[to be filled]

**Plan:**

- Timm

## 26.11. Deep Dive into OSH; Entrepreneurship for OSH

- IP Law basics + role of OSH
- practical OSH examples
- How to make it open?
    - Documentation blocks (editable source files etc.)
    - tl;dr OSH licenses & compatibility
- Funding
- Open Entrepreneurship
- Side note: current efforts in the field: standardisation

**Plan:**

- Martin: IP Law [40min]
- _ventilate_
- Martin: How to make it open? [20min]
- _ventilate_
- Sigrid: Open Entrepreneurship [60min]
- _ventilate_
- Martin & Sigrid: Open value proposition [60min]
    - [group action: value proposition](group-actions/G-deep-value.md)
- Martin: DIN SPEC 3105 [15min]
- _ventilate_
- Daniel: DIN [30min]
